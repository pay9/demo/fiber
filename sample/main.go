package main

import (
	"log"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/template/html"
)

func main() {
	engine := html.New("./views", ".gohtml")
	//engine := html.NewFileSystem(http.Dir("./views"), ".html")
	engine.Reload(true)
	engine.Debug(true)
	engine.Layout("embed")
	engine.Delims("{{", "}}")
	engine.AddFunc("greet", func(name string) string {
		return "Hello, " + name + "!"
	})
	app := fiber.New(fiber.Config{
		Views: engine,
	})
	app.Get("/", func(c *fiber.Ctx) error {
		return c.Render("index", fiber.Map{
			"Title": "Hello, World!",
		})
	})
	app.Get("/layouts", func(c *fiber.Ctx) error {
		return c.Render("index", fiber.Map{
			"Title": "Hello World!",
		}, "layouts/main")
	})
	app.Get("/donate", func(c *fiber.Ctx) error {
		return c.Render("donate", fiber.Map{
			"Title": "Pay9/Donate",
		})
	})
	app.Get("/landing", func(c *fiber.Ctx) error {
		return c.Render("landing", fiber.Map{
			"Title": "Pay9/Donate",
		})
	})
	app.Static("/", "./media")
	log.Fatal(app.Listen(":3000"))
}
